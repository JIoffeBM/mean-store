# Mock Online Store #
## Using TypeScript, MEAN, and Webpack ##

This is a sample architecture and implementation of TypeScript using a MEAN stack. Webpack is used to bundle both the SERVER and CLIENT portions of the application.

Note: The client-side uses a debug config and may be larger than the output for production. 

To use, simple clone the repository and run **npm install** from the application root.
To build and run, execute **npm start**

* MongoDB (via Mongoose) is used to maintain a database of products
* The cart is implemented through local storage