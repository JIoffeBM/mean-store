//TODO - REFACTOR WITH STRICT TYPING eg. for promises

import * as mongoose from 'mongoose';
import { ModelDefinitions } from '../models/db/model-includes.mongodb';
import { IModelDefinition } from '../models/db/model-definition.mongodb';

export interface IDBConnection{
    //open(): void;
}

const DEFAULT_CONNECTION_STRING = 'mongodb://localhost:27017/test';

export enum ConnectionStatus{
    CONNECTION_STATUS_OK,
    CONNECTION_STATUS_DISCONNECTED,
    CONNECTION_STATUS_ERROR,
    CONNECTION_NEVER_STARTED
}

export class MongoDBConnection implements IDBConnection{
    private _status: ConnectionStatus = ConnectionStatus.CONNECTION_NEVER_STARTED;
    private _modelMap: Object;
    private _connectionStatus: number;

    constructor(){
        this._modelMap = {};
        this.start().then( () => this.initializeModels() );
    }

    update(modelName: string, properties: Object, where: Object){
        let model = this._modelMap[modelName];
        if(!model){
            console.error('Tried to perform update using unsupported model name: ' + modelName);
        }

        return model.findOne(where).exec( (err, doc) => {
            if(!!err){
                console.error(err);
                return;
            }

            if(!doc){
                console.warn('No records found to update ' + modelName + ' where ' + JSON.stringify(where));
                return null;
            }

            Object.keys(properties).forEach( key => doc[key] = properties[key]);
            return doc.save();
        });
    }

    remove(modelName: string, where: Object){
        let model = this._modelMap[modelName];
        if(!model){
            console.error('Tried to perform a removal using an unsupported model name: ' + modelName);
        }

        return model.remove(where);
    }
    create(modelName: string, properties: Object){
        let model = this._modelMap[modelName];
        if(!model){
            console.error('Tried to perform creation using unsuppported model name: ' + modelName);
            return null;
        }

        let instance = new model(properties);
        return instance.save();
    }
    
    find(modelName: string){
        let model = this._modelMap[modelName];
        if(!model){
            console.error('Tried to perform query using unsuppport model name: ' + modelName);
            return null;
        }

        return model.find().lean();
    }

    getStatus() : ConnectionStatus{
        return this._connectionStatus;
    }

    isOpen(): boolean{
        return this.getStatus() === ConnectionStatus.CONNECTION_STATUS_OK;
    }

    start() : mongoose._mongoose._MongooseThenable{
        let connectionAttempt = mongoose.connect(DEFAULT_CONNECTION_STRING);
        let con = mongoose.connection;
        con.on('open', () => { 
            console.log('Established MongooseJS connection to: ' + DEFAULT_CONNECTION_STRING);
            this._connectionStatus = ConnectionStatus.CONNECTION_STATUS_OK;
        });
        con.on('error', (err) => { 
            console.error('MongooseJS Connection Error', err);
            this._connectionStatus = ConnectionStatus.CONNECTION_STATUS_ERROR;
        });
        con.on('disconnected', () => {
            console.info('MongooseJS connection to ' + DEFAULT_CONNECTION_STRING + ' has been closed');
            this._connectionStatus = ConnectionStatus.CONNECTION_STATUS_DISCONNECTED;
        });
        return connectionAttempt;
    }
    shutdown(): MongoosePromise<void>{
        return mongoose.connection.close();
    }

    private addModel(modelDef: IModelDefinition){
        let name = modelDef.name;
        let schema = modelDef.schemaDef;

        this._modelMap[name] = mongoose.model(name, new mongoose.Schema(schema));
    }

    private initializeModels(): void{
        ModelDefinitions.forEach((modelDef) => this.addModel(modelDef));
    }
}