import * as express from 'express';
import { MongoDBConnection } from '../services/db-connection';
import { IProduct } from '../../shared/models/product.model';
import { App } from '../app';

//Hook up router for web services
const routePath = '/api';
const routeListing = __dirname + '/app/server/routers/api.router.listing.html';

export class HttpRouter{
    private _router: express.Router;

    public getRouter(): express.Router{
        return this._router;
    }

    public getRoutePath(): string{
        return routePath;
    }

    constructor(app: App){
        this._router = express.Router();
        let router = this._router;

        /*
            GET - ALL products
                  available query params:
                  name - partial match
            POST - CREATE NEW PRODUCT using any properties except ID
        */
        router.route('/products')
            .post( (req: express.Request, res: express.Response) => {
                let dbConnection = app.getDBConnection();

                if(!dbConnection.isOpen())
                    return this.onInvalidConnection(req, res);

                dbConnection.find('Product')
                .sort({id: -1})
                .limit(1)
                .exec( (err, products) => {
                    if(!!err)
                        return this.onError(req, res, err);

                    //The new ID will either be 1 more than the maximum ID or 1 if there are no entries
                    let newId = !!products.length ? (products[0].id + 1) : 1;
                    let product: IProduct = {
                        name: req.body.name || 'Untitled',
                        price: req.body.price || 0,
                        id: newId
                    }

                    dbConnection.create('Product', product).then( () => {
                        res.setHeader('Content-Type', 'application/json');
                        res.json({message: 'Created new product'});
                        res.end();
                    });
                });

            })
            .get( (req: express.Request, res: express.Response) => {
                let dbConnection = app.getDBConnection();

                if(!dbConnection.isOpen())
                    return this.onInvalidConnection(req, res);

                let query = dbConnection.find('Product');

                if(!!req.query.name){
                    query = query.where('name').equals(new RegExp('.*' + req.query.name + '.*', 'i'));
                }

                query.exec( (err, products) => {
                    if(!!err)
                        return this.onError(req, res, err);

                    res.setHeader('Content-Type', 'application/json');
                    res.json(products);
                    res.end();
                } );
            });

        /*
            GET - SINGLE PRODUCT BY ID
            PUT - UPDATE PRODUCT AT ID
            DELETE - REMOVE PRODUCT AT ID
        */
        router.route('/products/:id')
        .get( (req: express.Request, res: express.Response) => {
            let dbConnection = app.getDBConnection();

            if(!dbConnection.isOpen())
                return this.onInvalidConnection(req, res);           

            dbConnection.find('Product')
            .where('id')
            .equals(req.params.id)
            .exec( (err, products) => {
                if(!!err)
                    return this.onError(req, res, err);

                res.setHeader('Content-Type', 'application/json');
                res.json(products);
                res.end();
            } );                
        })
        .put( (req: express.Request, res: express.Response) => {
            let dbConnection = app.getDBConnection();

            if(!dbConnection.isOpen())
                return this.onInvalidConnection(req, res);
   
            dbConnection.update('Product', req.body, {id: parseInt(req.params.id)})
            .then( () => {
                res.setHeader('Content-Type', 'application/json');
                res.json({message: 'Updated product ID ' + req.params.id});
                res.end();
            });            
        })
        .delete( (req: express.Request, res: express.Response) => {
            let dbConnection = app.getDBConnection();
            
            if(!dbConnection.isOpen())
                return this.onInvalidConnection(req, res);
                            
            dbConnection.remove('Product', {id: parseInt(req.params.id)})
            .then( () => {
                res.setHeader('Content-Type', 'application/json');
                res.json({message: 'Removed product ID ' + req.params.id});
                res.end();                
            });
        });

        //When accessing the api root, output a listing of supported endpoints
        router.get('/', (req: express.Request, res: express.Response) => res.sendFile(routeListing));
    }

    private onError(req: express.Request, res: express.Response, err): void{
        console.error(err);

        res.status(500)
        .send({error: 'Could not process your request at this time. Please try again later.'})
        .end();
    }

    private onInvalidConnection(req: express.Request, res: express.Response){
        return this.onError(req, res, {error: 'MongoDB Connection is not open'});
    }
}

