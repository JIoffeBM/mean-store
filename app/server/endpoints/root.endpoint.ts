import { IEndPoint } from './endpoint.base.ts';

/*
    If a get request isn't handled by a particular route, forward the index page by default'
*/
export let EndPoint: IEndPoint = {
    path: '*',
    callback: (req, res, app) => {
        res.sendFile(app.getDefaultPage());
    }
}