import { IEndPoint } from './endpoint.base.ts';

/*
    Redirect .html requests to the heart of the app
*/
export let EndPoint: IEndPoint = {
    path: /\.html$/,
    callback: (req, res, app) => {
        res.redirect('/');
    }
}