export interface IEndPoint{
    path: string|RegExp,
    callback: (req, res, app) => void
}