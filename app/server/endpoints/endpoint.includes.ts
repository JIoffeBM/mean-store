import { EndPoint as Root } from './root.endpoint';
import { EndPoint as HtmlRedirect } from './html.endpoint';

export let EndPoints = [
    Root //Root should be the last endpoint
]