export interface IModelDefinition{
    name: string,
    schemaDef: Object
}