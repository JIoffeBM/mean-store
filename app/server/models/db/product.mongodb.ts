import { IModelDefinition } from './model-definition.mongodb';

export let Product: IModelDefinition = {
    name: 'Product',
    schemaDef: {
        name: String,
        id: Number,
        price: Number
    }
}