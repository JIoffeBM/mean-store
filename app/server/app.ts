import * as express from 'express';
import * as bodyParser from 'body-parser';
import { MongoDBConnection } from './services/db-connection';
import { HttpRouter } from './routers/api.router';
import { EndPoints } from './endpoints/endpoint.includes';
import { IEndPoint } from './endpoints/endpoint.base';
import { ErrorHandlers } from './middleware/error-handlers.middleware';

const DEFAULT_PAGE = __dirname + '/public/index.html';
const DEFAULT_PORT = 3000;

export class App{
    private _port = DEFAULT_PORT;
    private _app: express.Application;
    private _server;
    private _dbConnection: MongoDBConnection;
    private _httpRouter: HttpRouter;

    constructor(getTargets: IEndPoint[]){
        this._app = express();

        this.initializeDBConnection()
        this.initializeStaticRoutings();
        this.initializeServicesRouter();
        this.initializeErrorHandlers();

        if(!!getTargets)
            getTargets.forEach( target => this._app.get( target.path, (req, res) => target.callback(req, res, this)) );

        //Hook in process shutdown cleanup code
        process.on('SIGINT', () => {
            console.info('Shutting down server on port ' + this._port + '...');
            this._server.close();
            this._dbConnection.shutdown().then(()=>process.exit());
        });                
    }

    public start(): void{
        if(!this._app){
            console.error('Invalid express instance!');
            return;
        }
        
        this._server = this._app.listen(this._port, () => console.log('Listening on port ' + this._port) );  
    }

    public getDBConnection(): MongoDBConnection{
        return this._dbConnection;
    }

    public getDefaultPage(){
        return DEFAULT_PAGE;
    }

    public getDirectory(){
        return __dirname;
    }

    private initializeDBConnection(){
        this._dbConnection = new MongoDBConnection();
    }
    private initializeServicesRouter(){
        //Use the body-parser library to enable parsing of
        //POST form data and JSON in request bodies
        this._app.use(bodyParser.urlencoded({ extended: false }));
        this._app.use(bodyParser.json());

        this._httpRouter = new HttpRouter(this);
        this._app.use(this._httpRouter.getRoutePath(), this._httpRouter.getRouter());
    }

    private initializeErrorHandlers(){
        ErrorHandlers.forEach(handler => this._app.use(handler));
    }

    private initializeStaticRoutings(){
        this._app.use(express.static('public'));
        this._app.use('/assets/npm/', express.static('node_modules'));
        this._app.use('/assets/release/', express.static('build'));
        this._app.use('/client-templates/', express.static('app/client'));
    }
}

export let AppInstance = new App(EndPoints);

AppInstance.start();