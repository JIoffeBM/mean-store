import * as express from 'express';

const logErrors = (err, req: express.Request, res: express.Response, next: express.NextFunction) => {
    console.error(err.stack);
    next(err);
}    

const errorResponseHandler = (err, req: express.Request, res: express.Response, next: express.NextFunction) => {
    res.status(500)
    .send({ error: 'There was an error processing your request, please try again later'})
    .end();
}    
export const ErrorHandlers = [
    logErrors,
    errorResponseHandler
];