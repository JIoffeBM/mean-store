import { module, IComponentOptions } from 'angular';
import { ModuleName } from '../app';
import { IProduct } from '../../shared/models/product.model';
import { IProductService } from '../services/products.base.service';
//import { ServiceName as productServiceName } from '../services/products-local-storage.service';
import { ServiceName as productServiceName } from '../services/products-web.service';

let componentName = 'bmSearchResults';

class SearchComponentController{
    q: string;
    products: IProduct[];

    //Define dependencies for DI, which are automatically passed
    //in order through the constructor below
    static $inject = [
        productServiceName
    ];

    constructor(private productService: IProductService) { }

    $routerOnActivate(next) {
        this.q = next.params.q;

        this.productService.getProductsByName(this.q).then(data => this.products = data);
    }
}

let adminComponent: IComponentOptions = {
    templateUrl: 'client-templates/search-results/search-results.component.html',
    controller: SearchComponentController,
    controllerAs: 'searchResults'
};

module(ModuleName).component(componentName, adminComponent);

export const ComponentName = componentName;