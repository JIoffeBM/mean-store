import { module, IComponentOptions} from 'angular';
import { ModuleName } from '../app';
import { IProduct } from '../../shared/models/product.model';

let componentName = 'bmSearchWidget';


class SearchWidgetComponentController {
    q: string = '';
}

let searchWidgetComponent: IComponentOptions = {
    templateUrl: 'client-templates/search-widget/search-widget.component.html',
    controller: SearchWidgetComponentController,
    controllerAs: 'searchWidget'
};

module(ModuleName).component(componentName, searchWidgetComponent);

export const ComponentName = componentName;