import { module, IComponentOptions, IPromise } from 'angular';
import { ModuleName } from '../app';

import { IProductService, IProductMap } from '../services/products.base.service';
// import { ServiceName as productServiceName } from '../services/products-local-storage.service';
import { ServiceName as productServiceName } from '../services/products-web.service';
import { ServiceName as cartServiceName, ICartService, ICartSession } from '../services/cart.service';

let componentName = 'bmCart';

class CartComponentController{
    cartSession: ICartSession;
    productMap: IProductMap;
    isEmpty: boolean = true;

    static $inject = [
        productServiceName,
        cartServiceName
    ];

    constructor(private productService: IProductService, private cartService: ICartService) {}

    $routerOnActivate() {
        this.cartService.getSession().then( cartSession => this.updateCart(cartSession) )
        this.productService.getProductMap().then( productMap => this.productMap = productMap );
    }

    onProductRemove(id: string): IPromise<ICartSession>{
        return this.cartService.removeProduct(id).then( cartSession => this.updateCart(cartSession) );
    }

    onCartEmpty(): IPromise<ICartSession>{
        return this.cartService.clear().then( cartSession => this.updateCart(cartSession) );
    }

    private updateCart(cartSession: ICartSession): ICartSession{
        this.cartSession = cartSession;
        this.isEmpty = Object.keys(cartSession.products).length === 0;
        return cartSession;
    }
}
let cartComponent: IComponentOptions = {
    templateUrl: 'client-templates/cart/cart.component.html',
    controller: CartComponentController,
    controllerAs: 'cart'
};

module(ModuleName).component(componentName, cartComponent);

export const ComponentName = componentName;