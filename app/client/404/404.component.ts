import { module, IComponentOptions } from 'angular';
import { ModuleName } from '../app';

let componentName = 'bmInvalidPage';

class ProductComponentController {
    $routerOnActivate(next) {
    }
}

let invalidPageComponent: IComponentOptions = {
    templateUrl: 'client-templates/404/404.component.html',
    controller: ProductComponentController,
    controllerAs: 'invalidPage'
};

module(ModuleName).component(componentName, invalidPageComponent);

export const ComponentName = componentName;