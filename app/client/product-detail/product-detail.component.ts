import { module, IComponentOptions, IPromise } from 'angular';
import { ModuleName } from '../app';
import { IProduct } from '../../shared/models/product.model';
import { IProductService } from '../services/products.base.service';
//import {ServiceName as productServiceName } from '../services/products-local-storage.service';
import {ServiceName as productServiceName } from '../services/products-web.service';
import {ServiceName as cartServiceName, ICartService, ICartSession } from '../services/cart.service';
import {ComponentName as appRouterComponentName} from '../app-router/app-router.component';

let componentName = 'bmProduct';

class ProductComponentController {
    product: IProduct;
    numberInCart: number = 0;
    cartSession: ICartSession;

    static $inject = [
        productServiceName,
        cartServiceName
    ];

    constructor(private productService: IProductService, private cartService: ICartService) {}

    $routerOnActivate(next) {
        this.productService.getProductById(next.params.id)
        .then(product => this.product = product)
        .then( () => this.cartService.getSession() )
        .then( cartSession => this.updateCart(cartSession) ); 
    }

    public onAddToCart(): IPromise<ICartSession>{
        return this.cartService.addProduct(this.product.id + '')
        .then( cartSession => this.updateCart(cartSession) );
    }

    private updateCart(cartSession: ICartSession): ICartSession{
        this.cartSession = cartSession;
        this.numberInCart = cartSession.products[this.product.id] || 0;
        return cartSession;
    }
}

let productComponent: IComponentOptions = {
    templateUrl: 'client-templates/product-detail/product-detail.component.html',
    controller: ProductComponentController,
    controllerAs: 'productDetail'
};

module(ModuleName).component(componentName, productComponent);

export const ComponentName = componentName;