import { module, IComponentOptions } from 'angular';
import { ModuleName } from '../app';

let componentName = 'bmHomePage';

class HomepageComponentController {
    $routerOnActivate(next) {
    }
}

let homepageComponent: IComponentOptions = {
    templateUrl: 'client-templates/homepage/homepage.component.html',
    controller: HomepageComponentController,
    controllerAs: 'homepage'
};

module(ModuleName).component(componentName, homepageComponent);

export const ComponentName = componentName;