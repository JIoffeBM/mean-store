import { module, IComponentOptions } from 'angular';
import { ModuleName } from '../app';
import { ComponentName as productListComponentName } from '../product-list/product-list.component';
import { ComponentName as adminComponentName } from '../admin/admin.component';
import { ComponentName as productDetailComponentName } from '../product-detail/product-detail.component';
import { ComponentName as aboutComponentName } from '../about/about.component';
import { ComponentName as searchResultsComponentName } from '../search-results/search-results.component';
import { ComponentName as cartComponentName } from '../cart/cart.component';
import { ComponentName as invalidPageComponentName } from '../404/404.component';
import { ComponentName as serverErrorComponentName } from '../server-error/server-error.component';
import { ComponentName as homepageComponentName } from '../homepage/homepage.component';

let componentName = 'bmAppRouter';

let appRouterComponent: IComponentOptions = {
    transclude: true,
    templateUrl: 'client-templates/app-router/app-router.component.html',
    $routeConfig: [
        { path: '/products', name: 'Products', component: productListComponentName },
        { path: '/', name: 'Homepage', component: homepageComponentName},
        { path: '/products/:id', name: 'Product', component: productDetailComponentName },
        { path: '/about', name: 'About', component: aboutComponentName },
        { path: '/admin', name: 'Admin', component: adminComponentName },
        { path: '/search', name:'Search', component: searchResultsComponentName },
        { path: '/search/:q', name:'Search', component: searchResultsComponentName },
        { path: '/cart', name: 'Cart', component: cartComponentName },
        { path: '/error', name: 'Error', component: serverErrorComponentName },
        { path: '/invalid', name: 'Invalid', component: invalidPageComponentName },
        { path: '/**', redirectTo: ['Invalid']}
    ]
};

module(ModuleName).component(componentName, appRouterComponent);

export const ComponentName = componentName;



