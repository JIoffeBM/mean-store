import { module, IQService, IPromise } from 'angular';
import { ModuleName } from '../app';
import { IProduct } from '../../shared/models/product.model';
import { IProductMap, IProductService } from './products.base.service';

const SERVICE_TARGET = '/api/products';

//Keep a list of supported keys to ensure data integrity
//When passing an array to and from an applied model,
//angular may add a few extra properties for keeping track
//of unique entries, etc.
const PROPERTIES = [
    'name',
    'price'
];

let serviceName = 'productWebService';

let emptyMap: IProductMap = {
    timestamp: null,
    map: {}
}

class ProductService implements IProductService {

    static $inject = [
        '$q',
        '$http'
    ];

    /*
        Interface Implementations
    */
    getProducts(): IPromise<IProduct[]> {
        return this.$http({
            method: 'GET',
            url: SERVICE_TARGET
        }).then( response => <IProduct[]>response.data, response => { 
            console.error(response.data.error); 
            return [];
        });
    }

    getProductMap(): IPromise<IProductMap>{
        return this.getProducts().then( products => {
            let productMap: IProductMap = {
                timestamp: new Date(),
                map: {}
            };
            let map = productMap.map;

            products.forEach( product => map[product.id] = product);

            return productMap;
        }, response => { 
            console.error(response.data.error);
            return emptyMap;
        } );
    }

    getProductById(id: string): IPromise<IProduct>{
        return this.$http({
            method: 'GET',
            url: SERVICE_TARGET + '/' + id
        }).then( response => <IProduct>response.data[0] || null, response => { 
            console.error(response.data.error);
            return null;
        });        
    }

    deleteProduct(product: IProduct): IPromise<IProduct>{
        return this.$http({
            method: 'DELETE',
            url: SERVICE_TARGET + '/' + product.id
        }).then( response => product || null, response => { 
            console.error(response.data.error);
            return null;
        });             
    }

    getProductsByName(name: string): IPromise<IProduct[]>{
        return this.$http({
            method: 'GET',
            url: SERVICE_TARGET,
            params: {name: name}
        }).then( response => <IProduct[]>response.data, response => { 
            console.error(response.data.error); 
            return [];
        });
    }

    createProduct(): IPromise<IProduct>{
        return this.$http({
                method: 'POST',
                url: SERVICE_TARGET,
                data: {name: 'New Product', price: 0}
        });
    }

    updateProduct(product: IProduct): IPromise<IProduct>{
        let properties = {};
        PROPERTIES.forEach( key => properties[key] = product[key] );

        return this.$http({
            method: 'PUT',
            url: SERVICE_TARGET + '/' + product.id,
            data: properties
        }).then( response => product );
    }

    constructor(private $q: IQService, private $http: ng.IHttpService) { }
}

module(ModuleName).service(serviceName, ProductService);

export const ServiceName = serviceName;