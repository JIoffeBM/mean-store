import { IPromise } from 'angular';
import { IProduct } from '../../shared/models/product.model';

/*
    The IProductMap stored on local storage consists of two objects:
    - a timestamp
    - an object whose keys correspond with the unique IDs of products, and values are products
*/
export interface IProductMap{
    timestamp: Object,
    map: Object
}

/*
    The interface exposed to the application includes the following
    self-explanitory functions.

    In this simple setting, 'get' methods will return null if the
    user agent does not support local storage. In the real world,
    an engineer can either check for validity of the returned data
    from the promise, use the optional syntax in Angular templating,
    or return a "dummy" object that will hold no information but
    prevent null pointer exceptions
*/
export interface IProductService {
    getProducts(): IPromise<IProduct[]>;
    getProductMap(): IPromise<IProductMap>;
    createProduct(): IPromise<IProduct>;
    updateProduct(product: IProduct);
    getProductById(id: string): IPromise<IProduct>;
    getProductsByName(name: string): IPromise<IProduct[]>;
    deleteProduct(product: IProduct): IPromise<IProduct>;
}