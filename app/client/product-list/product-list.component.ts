import { module, IComponentOptions } from 'angular';
import { ModuleName } from '../app';
import { IProduct } from '../../shared/models/product.model';
import { IProductService } from '../services/products.base.service';
// import { ServiceName as productServiceName } from '../services/products-local-storage.service';
import { ServiceName as productServiceName } from '../services/products-web.service';

let componentName = 'bmProductList';

class ProductListComponentController {

    static $inject = [
        productServiceName
    ];

    products: IProduct[];

    constructor(private productService: IProductService) { }

    $routerOnActivate() {
        this.productService.getProducts().then(value => this.products = value);
    }
}

let productListComponent: IComponentOptions = {
    templateUrl: 'client-templates/product-list/product-list.component.html',
    controller: ProductListComponentController,
    controllerAs: 'productList'
};

module(ModuleName).component(componentName, productListComponent);

export const ComponentName = componentName;