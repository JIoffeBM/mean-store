import { bootstrap, module, element, ILocationProvider, IHttpProvider } from 'angular';
import { ServerErrorInterceptor } from './interceptors/server-error.interceptor';
import '../../node_modules/@angular/router/angular1/angular_1_router';

let appName = 'app';
export const ModuleName = appName;

module(appName, ['ngComponentRouter'])
    .config(($locationProvider: ILocationProvider) => {
        $locationProvider.html5Mode(true);
    })
    .config(['$httpProvider', 
            '$locationProvider', 
            ($httpProvider: IHttpProvider, $locationProvider: ILocationProvider) => 
            $httpProvider.interceptors.push(ServerErrorInterceptor.Factory)
    ])
    .value('$routerRootComponent', 'bmAppRouter');

element(document).ready(() => bootstrap(document, [appName]));

import './app-router/app-router.component';
import './search-widget/search-widget.component';