import { module, IComponentOptions } from 'angular';
import { ModuleName } from '../app';

let componentName = 'bmServerError';

class ServerErrorComponentController {
    $routerOnActivate(next) {
    }
}

let serverErrorComponent: IComponentOptions = {
    templateUrl: 'client-templates/server-error/server-error.component.html',
    controller: ServerErrorComponentController,
    controllerAs: 'serverError'
};

module(ModuleName).component(componentName, serverErrorComponent);

export const ComponentName = componentName;