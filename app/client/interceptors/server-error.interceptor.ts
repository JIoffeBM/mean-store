import { module, IComponentOptions, IQService, ILocationService } from 'angular';
import { ModuleName } from '../app';

export class ServerErrorInterceptor{
    private static _instance: ServerErrorInterceptor;

    constructor(private $q: IQService, private $location: ILocationService){}

    public static Factory($q: IQService, $location: ILocationService){
        if(!ServerErrorInterceptor._instance)
            ServerErrorInterceptor._instance = new ServerErrorInterceptor($q, $location);

        return ServerErrorInterceptor._instance;
    }

    public response(response){
        return response || ServerErrorInterceptor._instance.$q.when(response);
    }
    public responseError(rejection){
        let self = ServerErrorInterceptor._instance;
        self.$location.path('/error').replace();
        return self.$q.reject(rejection);
    }
}