import { module, IComponentOptions, IPromise } from 'angular';
import { ModuleName } from '../app';
import { IProduct } from '../../shared/models/product.model';
import { IProductService } from '../services/products.base.service';
//import { ServiceName as productServiceName } from '../services/products-local-storage.service';
import { ServiceName as productServiceName } from '../services/products-web.service';

let componentName = 'bmAdmin';

class AdminComponentController{
    products: IProduct[];

    //Define dependencies for DI, which are automatically passed
    //in order through the constructor below
    static $inject = [
        productServiceName
    ];

    constructor(private productService: IProductService) { }

    $routerOnActivate() {
        this.getProducts();
    }    

    public getProducts(): IPromise<IProduct[]>{
        return this.productService.getProducts().then(value => this.products = value);
    }
    public createNewProduct(): IPromise<IProduct>{
        return this.productService.createProduct().then( product => {
            this.getProducts();
            return product;
         });
    }

    public saveProduct(product: IProduct): IPromise<IProduct>{
        return this.productService.updateProduct(product).then( product => {
            this.getProducts();
            return product;
        });
    }

    public deleteProduct(product: IProduct): IPromise<IProduct>{
        return this.productService.deleteProduct(product).then( product => {
            this.getProducts();
            return product; 
        });
    }
}

let adminComponent: IComponentOptions = {
    templateUrl: 'client-templates/admin/admin.component.html',
    controller: AdminComponentController,
    controllerAs: 'admin'
};

module(ModuleName).component(componentName, adminComponent);

export const ComponentName = componentName;