var fs = require('fs');

var nodeModules = {};
fs.readdirSync('node_modules').filter(function(x) {
    return ['.bin'].indexOf(x) === -1;
}).forEach(function(mod) {
    nodeModules[mod] = 'commonjs ' + mod;
});

var serverConfig = {
    entry: ['./app/server/app'],
    target: 'node',

    //Although it is counter-intuitive, this actually
    //tells webpack not to override node's own __dirname generation'
    node: {
        __dirname: false,
        __filename: false,
    },    
    output: {
        path: './',
        filename: 'server-start.js'
    },
    externals: nodeModules,
    devtool: 'source-map',
    //plugins: [new webpack.optimize.UglifyJsPlugin()],
    module: {
        loaders: [{ test: /\.ts$/, loader: 'ts' }]
    },
    resolve: {
        extensions: ['', '.webpack.js', '.web.js', '.ts', '.js']
    },    
};

var clientConfig = {
    entry: ['./app/client/app'],
    target: 'web',
    output: {
        path: './build/js',
        filename: 'client-start.js'
    },
    //externals: nodeModules,
    devtool: 'source-map',
    module: {
        loaders: [{ test: /\.ts$/, loader: 'ts' }]
    },
    resolve: {
        extensions: ['', '.webpack.js', '.web.js', '.ts', '.js']
    },        
}
module.exports = [
    serverConfig,
    clientConfig
];